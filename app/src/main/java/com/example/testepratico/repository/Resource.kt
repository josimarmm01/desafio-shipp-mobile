package com.example.testepratico.repository

class Resource<T>(
    val dado: T?,
    val erro: String? = null
)