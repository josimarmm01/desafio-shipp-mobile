package com.example.testepratico.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testepratico.model.Order
import com.example.testepratico.model.ReponseOrder
import com.example.testepratico.model.ResponseResume
import com.example.testepratico.model.Resume
import com.example.testepratico.retrofit.ChekoutWebClient

class CheckoutRepository(private val webclient: ChekoutWebClient = ChekoutWebClient()) {

    fun requestOrder(order: Order) :LiveData<Resource<ReponseOrder>> {

        val liveData = MutableLiveData<Resource<ReponseOrder>>()
        webclient.solicitaOrder(order, quandoSucesso = {
            liveData.value = Resource(it)
        }, quandoFalha = {
            liveData.value = Resource(null, it)
        })

    return liveData
    }

    fun requestResume(resume: Resume) :LiveData<Resource<ResponseResume>> {

        val liveData = MutableLiveData<Resource<ResponseResume>>()
        webclient.solicitaResume(resume, quandoSucesso = {
            liveData.value = Resource(it)
        }, quandoFalha = {
            liveData.value = Resource(null, it)
        })

        return liveData
    }

}