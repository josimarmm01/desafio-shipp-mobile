package com.example.testepratico.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testepratico.asynctask.BaseAsyncTask
import com.example.testepratico.database.dao.ICartaoDAO
import com.example.testepratico.model.Cartao

class CartaoRepository(private val dao: ICartaoDAO) {

    fun buscaInterno() : LiveData<List<Cartao>> {
        return dao.buscaTodos()
    }

    fun salvaInterno(cartao: Cartao): LiveData<Resource<Void?>> {
        val liveData = MutableLiveData<Resource<Void?>>()
        BaseAsyncTask(quandoExecuta = {
            dao.salva(cartao)
        }, quandoFinaliza = {
        }).execute()

        liveData.value = Resource(null)
        return liveData
    }

}