package com.example.testepratico.UI.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testepratico.R
import com.example.testepratico.UI.viewModel.ListaCartoesViewModel
import com.example.testepratico.UI.viewModel.ListaCartoesViewModelFactory
import com.example.testepratico.UI.adapter.ListaCartoesAdapter
import com.example.testepratico.database.AppDatabase
import com.example.testepratico.model.Cartao
import com.example.testepratico.repository.CartaoRepository
import com.example.testepratico.util.initDarkToolbar
import kotlinx.android.synthetic.main.activity_listar_cartoes.*
import kotlinx.android.synthetic.main.item_card_cartao.*
import kotlinx.android.synthetic.main.item_card_novo_cartao.*
import org.jetbrains.anko.startActivity


class ListarCartoesActivity : AppCompatActivity() {

    private val viewModel by lazy {
        val repository = CartaoRepository(AppDatabase.getInstance(this).caraoDAO)
        val factory = ListaCartoesViewModelFactory(repository)
        val provedor = ViewModelProvider( this, factory)
        provedor.get(ListaCartoesViewModel(repository)::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar_cartoes)

        initToolbar()
        configuraRecyclerView()
        buscaCartoes()
        configuraBotaoAdicionaCartao()
    }

    override fun onResume() {
        super.onResume()
        buscaCartoes()
    }

    private fun initToolbar() {
        initDarkToolbar(this, toolbar, getString(R.string.cartoes_titulo), true)
    }

    private fun configuraRecyclerView() {
        lista_cartao_recyclerview.layoutManager = LinearLayoutManager(this)

        val onClickListener: (Cartao) -> (Unit) = { cartao ->
            selecionaCatao(cartao)
        }

        lista_cartao_recyclerview.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        lista_cartao_recyclerview.adapter = ListaCartoesAdapter(this, onClickListener)
    }

    private fun buscaCartoes() {

        viewModel.buscaTodos().observe(this, Observer {cartoes ->
            if(cartoes.isNotEmpty()) {
                (lista_cartao_recyclerview.adapter as ListaCartoesAdapter).updateList(cartoes)
                icone_cartao_vazio.visibility = View.GONE
                label_cartoes_vazio.visibility = View.GONE
            } else {
                icone_cartao_vazio.visibility = View.VISIBLE
                label_cartoes_vazio.visibility = View.VISIBLE
            }
        })
    }

    private fun configuraBotaoAdicionaCartao() {
        adicionar_novo_cartao.setOnClickListener {
            startActivity<AdicionarCartaoActivity>()
        }
    }

    private fun selecionaCatao(cartao: Cartao) {
        intent.putExtra("cartao", cartao)
        setResult(650, intent)
        finish()
    }
}
