package com.example.testepratico.UI.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.testepratico.model.Cartao
import com.example.testepratico.repository.CartaoRepository

class ListaCartoesViewModel (private val repository: CartaoRepository) : ViewModel() {

    fun buscaTodos() : LiveData<List<Cartao>> {
        return repository.buscaInterno()
    }

}
