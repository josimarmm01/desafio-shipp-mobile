package com.example.testepratico.UI.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.testepratico.repository.CartaoRepository

class ListaCartoesViewModelFactory(
    private val repository: CartaoRepository
): ViewModelProvider.Factory{
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ListaCartoesViewModel(repository) as T
    }
}