package com.example.testepratico.UI.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.testepratico.repository.CheckoutRepository

class ConfirmaPedidoViewModelFactory( private val repository: CheckoutRepository
): ViewModelProvider.Factory{
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ConfirmaPedidoViewModel(repository) as T
    }
}