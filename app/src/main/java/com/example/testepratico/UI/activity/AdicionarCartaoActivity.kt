package com.example.testepratico.UI.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.testepratico.R
import com.example.testepratico.UI.viewModel.AdicionaCartaoViewModel
import com.example.testepratico.UI.viewModel.AdicionaCartartaoViewModelFactory
import com.example.testepratico.database.AppDatabase
import com.example.testepratico.util.initDarkToolbar
import com.example.testepratico.model.Cartao
import com.example.testepratico.repository.CartaoRepository
import com.example.testepratico.util.Consts
import kotlinx.android.synthetic.main.activity_adicionar_cartao.*
import kotlinx.android.synthetic.main.item_exibicao_cartao.*
import kotlinx.android.synthetic.main.item_formulario_cartao.*
import org.jetbrains.anko.toast

class AdicionarCartaoActivity : AppCompatActivity() {

    private val cartaoId: Long by lazy {
        intent.getLongExtra(Consts.CARTAO_ID_CHAVE, 0)
    }

    private val viewModel by lazy {
        val repository = CartaoRepository(AppDatabase.getInstance(this).caraoDAO)
        val factory = AdicionaCartartaoViewModelFactory(repository)
        val provedor = ViewModelProvider( this, factory)
        provedor.get(AdicionaCartaoViewModel(repository)::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adicionar_cartao)

        initToolbar()
        preencheAutomatico()
        adicionarCartao()
    }

    private fun initToolbar() {
        initDarkToolbar(this, toolbar, getString(R.string.adicionar_cartao_titulo), true)
    }

    private fun adicionarCartao() {

        button_adicionar_cartao.setOnClickListener {
            if (validaCampoCadastroCartao()) {
                preencheDadosCartao()
            }
        }
    }

    private fun validaCampoCadastroCartao(): Boolean {

        when {
            numero_cadastro_cartao.text.toString().isEmpty() -> {
                toast(R.string.cadastro_novo_cartao_numero_empty)
                return false
            }
            nome_cadastro_cartao.text.isEmpty() -> {
                toast(R.string.cadastro_novo_cartao_nome_empty)
                return false
            }
            data_cadastro_cartao.text.isEmpty() -> {
                toast(R.string.cadastro_novo_cartao_validade_empty)
                return false
            }
            cvv_cadastro_cartao.text.isEmpty() -> {
                toast(R.string.cadastro_novo_cartao_cvv_empty)
                return false
            }
            cpf_cadastro_cartao.text.toString().isEmpty() -> {
                toast(R.string.cadastro_novo_cartao_cpf_empty)
                return false
            }
            else -> {
                return true
            }
        }
}

    private fun preencheDadosCartao() {

        val cartao = Cartao(cartaoId,
            numero_cadastro_cartao.text.toString(),
            nome_cadastro_cartao.text.toString(),
            cvv_cadastro_cartao.text.toString(),
            data_cadastro_cartao.text.toString(),
            cpf_cadastro_cartao.text.toString())
        adicionarCartaoBanco(cartao)
    }

    private fun adicionarCartaoBanco(cartao: Cartao) {
        viewModel.salvaCartao(cartao).observe(this, Observer {
            if (it.erro == null) {
                finish()
            } else {
                toast(R.string.falha_adicionar_cartao)
            }
        })
    }

    private fun preencheAutomatico () {
        numero_cadastro_cartao.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                numero_cartao.text = s
            }
        })

        nome_cadastro_cartao.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                nome_cartao.text = s
            }
        })

        data_cadastro_cartao.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validade_cartao.text = s
            }
        })

        cvv_cadastro_cartao.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                cvv_cartao.text = s
            }
        })
    }

}
