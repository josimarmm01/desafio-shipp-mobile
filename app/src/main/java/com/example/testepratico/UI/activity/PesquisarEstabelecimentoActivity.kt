package com.example.testepratico.UI.activity

import android.Manifest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.testepratico.R
import com.example.testepratico.util.Consts
import com.example.testepratico.util.initDarkToolbar
import com.example.testepratico.util.toast
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_pesquisar_estabelecimento.*
import org.jetbrains.anko.startActivity

class PesquisarEstabelecimentoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pesquisar_estabelecimento)

        initToolabar()
        permicaoLocalizacao()
        configuraAutoComplete()

    }

    private fun initToolabar() {
        initDarkToolbar(this, toolbar, getString(R.string.buscar_estabelecimento_titulo), false)
    }

    private fun permicaoLocalizacao() {

        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
            .withListener(object : MultiplePermissionsListener {

                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        toast("Você precesisa ativar a sua localização")
                    }
                }
                override fun onPermissionRationaleShouldBeShown(permissions: List<com.karumi.dexter.listener.PermissionRequest?>?, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun configuraAutoComplete() {

        Places.initialize(applicationContext, Consts.api_key)
        val placesClient = Places.createClient(this)

        val autocompleteFragment =
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                    as AutocompleteSupportFragment

        autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS))

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                startActivity<ConfirmarPedidoActivity>("nomeEstabelecimento" to place.name,
                    "enderecoEstabelecimento" to place.address,
                    "latitudeEstabelecimento" to place.latLng?.latitude,
                    "logitudeEstabelecimento" to place.latLng?.longitude)

            }

            override fun onError(status: Status) {
                toast("Um erro ocorreu")
            }
        })
    }
}
