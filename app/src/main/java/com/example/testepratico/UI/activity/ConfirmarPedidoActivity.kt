package com.example.testepratico.UI.activity

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.example.testepratico.R
import com.example.testepratico.UI.adapter.InfoPedidoPagerAdapter
import com.example.testepratico.UI.fragments.DescricaoPedidoFragment
import com.example.testepratico.UI.fragments.ValorPedidoFragment
import com.example.testepratico.UI.viewModel.ConfirmaPedidoViewModel
import com.example.testepratico.UI.viewModel.ConfirmaPedidoViewModelFactory
import com.example.testepratico.model.Cartao
import com.example.testepratico.model.Order
import com.example.testepratico.model.Resume
import com.example.testepratico.repository.CheckoutRepository
import com.example.testepratico.util.initDarkToolbar
import com.example.testepratico.util.toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_confirmar_pedido.*
import kotlinx.android.synthetic.main.fragment_informacao_descricao_pedido.*
import kotlinx.android.synthetic.main.fragment_informacao_valor_pedido.*
import kotlinx.android.synthetic.main.item_card_buscar_cartao.*
import kotlinx.android.synthetic.main.item_card_pedido.*
import org.jetbrains.anko.startActivityForResult

class ConfirmarPedidoActivity : AppCompatActivity() {

    private val viewModel by lazy {
        val repository = CheckoutRepository()
        val factory = ConfirmaPedidoViewModelFactory (repository)
        val provedor = ViewModelProvider( this, factory)
        provedor.get(ConfirmaPedidoViewModel(repository)::class.java)
    }

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var latitudeEstabelecimento: Double? = null
    private var longitudeEstabelecimento: Double? = null
    private var latituteUsuario: Double? = null
    private var longituteUsuario: Double? = null

    private var cartao: Cartao? = null
    private lateinit var order: Order
    private lateinit var resumo: Resume

    var posicaoViewPAgeAtual: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmar_pedido)

        initTollbar()
        getLocation()
        initVoltar()
        initEstabelecimento()
        initAvancar()
        adicionarCarao()
        confirmarPedido()
        buttonOk()
        initViewpage()
    }

    private fun initTollbar() {
        initDarkToolbar(this, toolbar,getString(R.string.confirmar_pedido_titulo), true)
    }

    private fun enviarPedido(valor: Double) {

        order = Order(latitudeEstabelecimento?:0.0, longitudeEstabelecimento?:0.0,
            latituteUsuario?:0.0, longituteUsuario?:0.0,
            cartao?.cardNumber?: "", cartao?.cvv?: "", cartao?.expiryDate?: "",
            valor?: 0.0)

            viewModel.requestOrder(order).observe(this, Observer { response ->
            if (response.dado != null) {
                card_adiciona_cartao.visibility = View.GONE
                button_finalizar.visibility = View.GONE
                button_ok.visibility = View.VISIBLE
                toast(response.dado.message)
                toolbar.title = getString(R.string.pdido_confirmado_titulo)
            }
        })
    }

    private fun initEstabelecimento() {
        if (intent.hasExtra("nomeEstabelecimento")) {
            label_nome_local.text = intent.getStringExtra("nomeEstabelecimento")
        }
        if (intent.hasExtra("enderecoEstabelecimento")) {
            label_endereco_local.text = intent.getStringExtra("enderecoEstabelecimento")
        }
        if (intent.hasExtra("latitudeEstabelecimento")) {
            latitudeEstabelecimento = intent.getDoubleExtra("latitudeEstabelecimento", 0.0)
        }
        if (intent.hasExtra("logitudeEstabelecimento")) {
            longitudeEstabelecimento = intent.getDoubleExtra("logitudeEstabelecimento", 0.0)
        }
    }

    private fun buscarResumo(valor: Double) {

        resumo = Resume(latitudeEstabelecimento?:0.0,
            longitudeEstabelecimento?:0.0,
            latituteUsuario?:0.0,
            longituteUsuario?:0.0,
            valor)

        viewModel.requestResume(resumo).observe(this, Observer { response ->
            if (response.dado != null)
            enviarPedido(response.dado.total_value)
        })
    }

    private fun validaCampoDescricao(): Boolean {
        val descricao = cadastro_descricao_pedido.text
        return if (descricao.isEmpty()) {
            toast(getString(R.string.descricao_pedido_empty))
            false
        } else {
            true
        }
    }

    private fun validaCampoPreco(): Boolean {
        val preco = cadastro_valo_pedido.text
        return if (preco.isEmpty()) {
            toast(getString(R.string.valor_pedido_empty))
            false
        } else {
            true
        }
    }

    private fun validaCartao(): Boolean {
        val cartao = numero_cartao.text.toString()
        return if (cartao.isEmpty()) {
            toast(getString(R.string.forma_pagamento_pedido_empty))
            false
        } else {
            true
        }
    }

    private fun adicionarCarao() {
        card_adiciona_cartao.setOnClickListener {
            startActivityForResult<ListarCartoesActivity>(650)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 650) {

            cartao = data?.extras?.getSerializable("cartao") as Cartao

            label_adicionar_novo_cartao.visibility = View.GONE
            ic_parte_oculta.visibility = View.VISIBLE
            ic_cartao.visibility = View.VISIBLE

            numero_cartao.text = cartao!!.cardNumber
            numero_cartao.visibility = View.VISIBLE

        }
    }

    private fun getLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation.addOnSuccessListener { location : Location? ->
            location?.let {
                latituteUsuario = it.longitude
                longituteUsuario = it.latitude
            }
        }
    }

    private fun confirmarPedido() {

        button_finalizar.setOnClickListener {
            if (validaCartao()) {
                val valorSugerido = cadastro_valo_pedido.text.toString().toDouble()?: 0.0
                buscarResumo(valorSugerido)
            }
        }
    }

    private fun initAvancar() {

        button_avanca_pedido.setOnClickListener {
            if (posicaoViewPAgeAtual == 1 && validaCampoPreco()) {
                button_avanca_pedido.visibility = View.GONE
                button_finalizar.visibility = View.VISIBLE
                card_adiciona_cartao.visibility = View.VISIBLE
                viewpager_pedido.visibility = View.GONE
                cadastro_pedido_voltar.visibility = View.GONE
                linha_descricao.visibility = View.GONE
                label_valor_produto.text = cadastro_valo_pedido.text
                label_valor_cifra_produto.visibility = View.VISIBLE
            }

            if (posicaoViewPAgeAtual == 0 && validaCampoDescricao()) {
                viewpager_pedido.setCurrentItem(1, true)
                linha_preco.visibility = View.VISIBLE
                label_descricao_produto.text = cadastro_descricao_pedido.text
            }
        }
    }

    private fun initVoltar() {

        cadastro_pedido_voltar.setOnClickListener {
            if (posicaoViewPAgeAtual == 0) {
                onBackPressed()
            } else if (posicaoViewPAgeAtual == 1) {
                viewpager_pedido.setCurrentItem(0, true)
            }
        }
    }

    private fun buttonOk() {
        button_ok.setOnClickListener {
            finish()
        }
    }

    private fun initViewpage() {

        val descricaoPedidoFragment = DescricaoPedidoFragment()
        val precoPedidoFragment = ValorPedidoFragment()

        viewpager_pedido.adapter = InfoPedidoPagerAdapter(supportFragmentManager, lifecycle)
        viewpager_pedido.offscreenPageLimit = 1

        (viewpager_pedido.adapter as InfoPedidoPagerAdapter).addFragment(descricaoPedidoFragment)
        (viewpager_pedido.adapter as InfoPedidoPagerAdapter).addFragment(precoPedidoFragment)

        viewpager_pedido.isUserInputEnabled = false
        viewpager_pedido.registerOnPageChangeCallback(
            object: ViewPager2.OnPageChangeCallback() {

                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)

                    when (position) {
                        0 -> {
                            cadastro_pedido_voltar.visibility = View.GONE
                            button_avanca_pedido.visibility = View.VISIBLE
                            button_finalizar.visibility = View.GONE
                            posicaoViewPAgeAtual = position
                        }
                        1 -> {
                            cadastro_pedido_voltar.visibility = View.VISIBLE
                            button_avanca_pedido.visibility = View.VISIBLE
                            button_finalizar.visibility = View.GONE
                            posicaoViewPAgeAtual = position
                        }
                    }
                }
            } )
    }
}
