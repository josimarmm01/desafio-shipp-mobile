package com.example.testepratico.UI.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.testepratico.R
import com.example.testepratico.model.Cartao
import kotlinx.android.synthetic.main.item_adapter_cartao.view.*

class ListaCartoesAdapter(private val context: Context,
                          private val onClickListener: ((Cartao)->Unit)) : RecyclerView.Adapter<ListaCartoesAdapter.MyViewHolder> () {

    private var cartoes: MutableList<Cartao> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_adapter_cartao, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = cartoes.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val cartao: Cartao = cartoes[position]
        holder.bind(cartao)
        holder.itemView.setOnClickListener {
            onClickListener(cartao)
        }
    }

    fun updateList(list:List<Cartao>){
        cartoes.clear()
        cartoes.addAll(list)
        notifyDataSetChanged()
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val numero = itemView.numero_cartao

        fun bind(item: Cartao) {
            numero.text = item.cardNumber
        }
    }
}