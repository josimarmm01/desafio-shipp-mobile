package com.example.testepratico.UI.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.testepratico.model.Order
import com.example.testepratico.model.ReponseOrder
import com.example.testepratico.model.ResponseResume
import com.example.testepratico.model.Resume
import com.example.testepratico.repository.CheckoutRepository
import com.example.testepratico.repository.Resource

class ConfirmaPedidoViewModel(private val repository: CheckoutRepository) : ViewModel() {

    fun requestOrder(order: Order): LiveData<Resource<ReponseOrder>> {
        return repository.requestOrder(order)
    }

    fun requestResume(resume: Resume): LiveData<Resource<ResponseResume>> {
        return repository.requestResume(resume)
    }

}