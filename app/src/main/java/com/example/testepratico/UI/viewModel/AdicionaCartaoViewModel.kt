package com.example.testepratico.UI.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.testepratico.model.Cartao
import com.example.testepratico.repository.CartaoRepository
import com.example.testepratico.repository.Resource

class AdicionaCartaoViewModel(private val repository: CartaoRepository) : ViewModel() {

    fun salvaCartao(cartao: Cartao) : LiveData<Resource<Void?>> {
        return repository.salvaInterno(cartao)
    }

}