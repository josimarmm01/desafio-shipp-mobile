package com.example.testepratico.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Cartao(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val cardNumber: String,
    val cardName: String,
    val cvv: String,
    val expiryDate: String,
    val cpf: String
): Serializable