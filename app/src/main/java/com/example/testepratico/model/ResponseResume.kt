package com.example.testepratico.model

data class ResponseResume(
    val product_value: Double,
    val distance: Double,
    val total_value: Double,
    val fee_value: Double
)