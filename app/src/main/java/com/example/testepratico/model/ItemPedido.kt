package com.example.testepratico.model

data class ItemPedido(val label: String ,
                      val descricao: String)