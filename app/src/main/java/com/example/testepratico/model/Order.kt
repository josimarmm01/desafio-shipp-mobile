package com.example.testepratico.model

data class Order(
    val store_latitude: Double,
    val store_longitude: Double,
    val user_latitude: Double,
    val user_longitude: Double,
    val card_number: String,
    val cvv: String,
    val expiry_date: String,
    val value: Double
)