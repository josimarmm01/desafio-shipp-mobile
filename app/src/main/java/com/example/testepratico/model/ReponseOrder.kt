package com.example.testepratico.model

data class ReponseOrder(
    val message: String,
    val value: Double
)