package com.example.testepratico.model

data class Resume(
    val store_latitude: Double,
    val store_longitude:Double,
    val user_latitude:Double,
    val user_longitude:Double,
    val value: Double
)