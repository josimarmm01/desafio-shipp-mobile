package com.example.testepratico.retrofit

import com.example.testepratico.model.Order
import com.example.testepratico.model.ResponseResume
import com.example.testepratico.model.ReponseOrder
import com.example.testepratico.model.Resume
import com.example.testepratico.retrofit.service.ICheckoutService
import com.example.testepratico.util.Consts
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChekoutWebClient(private val service: ICheckoutService = AppRetrofit().checkoutService) {
    private fun <T> executaRequisicao(
        call: Call<T>,
        quandoSucesso: (sucesso: T?) -> Unit,
        quandoFalha: (erro: String?) -> Unit
    ) {
        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    quandoSucesso(response.body())
                } else {
                    quandoFalha(Consts.REQUISICAO_NAO_SUCEDIDA)
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                quandoFalha(t.message)
            }
        })
    }

    fun solicitaOrder(order: Order, quandoSucesso: (responseOrder: ReponseOrder?) -> Unit,
                      quandoFalha: (erro: String?) -> Unit) {
        executaRequisicao(service.requestOrder(order), quandoSucesso, quandoFalha)
    }

    fun solicitaResume(resume: Resume, quandoSucesso: (reponseResume: ResponseResume?) -> Unit,
                       quandoFalha: (erro: String?) -> Unit) {
        executaRequisicao(service.requestResume(resume), quandoSucesso, quandoFalha)
    }


}