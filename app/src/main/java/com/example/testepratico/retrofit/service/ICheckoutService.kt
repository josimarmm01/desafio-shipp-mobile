package com.example.testepratico.retrofit.service

import com.example.testepratico.model.ReponseOrder
import com.example.testepratico.model.Order
import com.example.testepratico.model.ResponseResume
import com.example.testepratico.model.Resume
import retrofit2.http.POST

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers

interface ICheckoutService {

    @Headers("Content-Type: application/json")
    @POST("challenge/v1/order")
    fun requestOrder(@Body order: Order): Call<ReponseOrder>

    @Headers("Content-Type: application/json")
    @POST("challenge/v1/order/resume")
    fun requestResume(@Body Resume: Resume): Call<ResponseResume>

}