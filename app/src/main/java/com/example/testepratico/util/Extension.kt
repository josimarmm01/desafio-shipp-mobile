package com.example.testepratico.util

import android.app.Activity
import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.example.testepratico.R

fun AppCompatActivity.initDarkToolbar(context: Context, toolbar : Toolbar, titulo: String? = "", back: Boolean){
    setSupportActionBar(toolbar)
    supportActionBar?.title = titulo?: ""
    toolbar.setTitleTextColor(ContextCompat.getColor(context,
        R.color.black
    ))
    if (back) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }
}

fun Activity.toast(mensagem: String) {
    Toast.makeText(this, mensagem, Toast.LENGTH_LONG).show()
}