package com.example.testepratico.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.testepratico.model.Cartao

@Dao
interface ICartaoDAO {

    @Query("SELECT * FROM Cartao ORDER BY id DESC")
    fun buscaTodos(): LiveData<List<Cartao>>

    @Insert(onConflict = REPLACE)
    fun salva(cartao: Cartao)

}