package com.example.testepratico.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.testepratico.database.dao.ICartaoDAO
import com.example.testepratico.model.Cartao

private const val NOME_BANCO_DE_DADOS = "cartao.db"

@Database(entities = [Cartao::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract val caraoDAO: ICartaoDAO

    companion object {

        private lateinit var db: AppDatabase

        fun getInstance(context: Context): AppDatabase {

            if(::db.isInitialized) return db

            db = Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                NOME_BANCO_DE_DADOS
            ).build()

            return db
        }
    }
}